#!/usr/bin/env ruby

# collect Capracotta "piazza Falconi" webcam images every few minutes
# assemble them to a 12 fps video to be uploaded on youtube, using:
#   ffmpeg -r 12 -f image2 -i %04d.jpg /tmp/youtube.mp4

# this script created https://youtu.be/b-smc5HI8mw

# an old version of this script (and an old version of the webcam
# output) was used to create https://youtu.be/O8vHVmp2iLY

DIR="/tmp/capracotta"
PAG="https://capracotta.com/capracotta/capracotta-webcam/"
TAG=/wp-content\/uploads\/webcam\/falconi\/.*jpg/
PAU=60

require 'net/http'

class String
  def log
    STDERR.print Time.now.strftime('%T ')
    STDERR.puts self
  end

  def save n
    if self.size > 0
      File.open(sprintf("%s/%04d.jpg", DIR, n), 'w').print(self)
      [ n+1, true ]
    else
      [ n, false ]
    end
  end

  def http
    r = ''
    begin
      r = Net::HTTP.get(URI(self))
    rescue Exception => e
      "http error: #{e}".log
    end
    r
  end

  def img
    r, z = '', ''
    if size > 0
      q = split(/\n/).grep(TAG)
      if q.size == 1
        begin
          z = q.first.split(/src."/).last.split(/"/).first
          r = Net::HTTP.get(URI(z))
        rescue Exception => e
          "get/parse error: #{e}".log
        end
      end
    end
    z.log
    r
  end
end


def pause
  pau = '/tmp/capracotta.txt'
  unless File.exist? pau
    # while images are generated every ~5 minutes,
    # we will allow some run-time adjustments
    # (just rewrite/erase the file to fix next download time)
    #
    open(pau, 'w').puts 297  # default: 300 seconds minus 3 seconds
  end
  sleep open(pau).gets.to_i
end


Dir.mkdir DIR
n = 1

while true
  pause  if n > 1

  # retry twice if anything goes wrong
  n, flag = PAG.http.img.save(n)
  n, flag = PAG.http.img.save(n)  unless flag
  n, flag = PAG.http.img.save(n)  unless flag
end

# ---
