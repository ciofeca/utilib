#!/usr/bin/env ruby

# GUI-less audio player using 'mplayer -slave' in background and keyboard multimedia key events

DEVICE='keyboard vkb'

def dev_event descr
	open('/proc/bus/input/devices').read.split("\n\n").each do |dev|
		info = {}
		dev.split("\n").each do |field|
			key = field.split('=').first
			info[key] = field[key.size + 1..-1].chomp.strip
		end
		if info['N: Name'].downcase.end_with? descr+'"'
			sysdev = '/sys' + info['S: Sysfs']
			dev = Dir[sysdev + '/event*']
			return "/dev/input/" + dev.first.split('/').last
		end
	end
	fail "not found: #{descr.inspect}"
end

fail "usage: #$0 file.mp3..."  if ARGV.size == 0

fpo = IO.popen %w{ mplayer -slave } + ARGV, 'w'
fpi = open(dev_event DEVICE)
    while true
	t1, t2, typ, cod, val = fpi.read(24).unpack("QQSSl")
	next  if typ != 1 || val != 1 || cod < 163 || cod > 166
	begin
		fpo.puts "seek +10"	if cod == 163
		fpo.puts "pause"	if cod == 164
		fpo.puts "seek -10"	if cod == 165
		fpo.puts "pt_step +1 1"	if cod == 166
	rescue Errno::EPIPE
		break
	end
end
