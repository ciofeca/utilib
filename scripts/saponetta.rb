#!/usr/bin/env ruby

class String
  def tag t
    "<#{t}>#{self}</#{t}>"
  end

  def mb
    ((self.to_i / 100_000) / 10.0).to_s
  end

  def gb
    ((self.to_i / 100_000_000) / 10.0).to_s
  end

  def datime
    t = self.to_i
    d = t / 86400
    s = t % 86400

    m = sprintf("%d:%02d:%02d", s / 3600, (s % 3600) / 60, s % 60)
    if d > 0
      "#{d} days + #{m}"
    else
      m
    end
  end

  def reqsap
    "<?xml version = \"1.0\" encoding=\"UTF-8\"?><request>#{self}</request>"
  end

  def cmdsap api
    `curl --silent --data-binary #{self.reqsap.inspect} http://3.home/api/#{api}`
  end

  def paramsap
    s = `curl --silent http://3.home/api/#{self}`.split("\n")
    h = { }
    s.each do |r|
      m = r.split(">").first.to_s
      next  if m == '<response' || m == '<?xml version="1.0" encoding="UTF-8"?'
      v = r.split("<")[1].split">"
      h[v.first] = v.last
    end
    h
  end
end

def loginsap                    # hardcoded default (eww): admin/admin
  (("admin".tag "Username") + ("dGFuZ28=".tag "Password")).cmdsap "user/login"
end

def rebootsap
  ("1".tag "Control").cmdsap "device/control"
end

def traffstat
  h = "monitoring/traffic-statistics".paramsap 
  upt = h['CurrentConnectTime']
  upt = upt.datime  if upt
  tct = h['TotalConnectTime']
  tct = tct.datime  if tct

  puts "!--uptime:  #{upt}  (out of #{tct})"
  puts "!--traffic: #{h['CurrentDownload'].mb} + #{h['CurrentUpload'].mb} Mb"
  puts "!--totals:  #{h['TotalDownload'].gb} + #{h['TotalUpload'].gb} Gb"

  h = "monitoring/status".paramsap
  puts "!--address: #{h['WanIPAddress']}"
end


# --- main
#
traffstat

cmd = ARGV.first

if cmd == 'login'
  loginsap
elsif cmd == 'reboot'
  loginsap
  rebootsap
  puts "!-- rebooting now"
elsif cmd
  puts "!--usage:   #{$0} [ login | reboot ]"
end

