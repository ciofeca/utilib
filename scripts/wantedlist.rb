#!/usr/bin/env ruby

# --- configurazione e personalizzazioni

# codici pezzi da non mettere mai nella wanted list
IGNORELIST = [ "91968", "3814" ]

# sostituzione pezzi introvabili con equivalenti economici, ed "alternate names" con codici bricklink
SUBSTLIST = { "6141" => "4073",
              "93888" => "3007",
              "4460" => "4460a",
              "6556" => "4033",
	      "30499" => "3684c",
	      "3062" => "44302",
	      "44237" => "2456",
	      "50746" => "54200",
	      "3070" => "3070b",     # tile 1x1 with groove, perché quello without non lo fanno dal 1976
	      "3068" => "3068b",     # tile 2x2 with groove, perché quello without non lo fanno dal 1978
	      "3069" => "3069b",     # tile 1x2 with groove, perché quello without non lo fanno dal 1979
	      "2412" => "2412b",     # grille 1x2 with groove, perché quello without non lo fanno dal 1997
            }

# default per gli acquisti: ""=qualsiasi, "U"=usato, "N"=nuovo
CONDDEFAULT=""

# colore convenzionale da ignorare in ogni caso
IGNORECOLOR="44"    # Transparent Yellow

# codici pezzi da comprare esclusivamente usati causa costi apocalittici
NEEDUSED=[ "4033", "4034", "4035", "4036" ]

# codici pezzi da comprare esclusivamente nuovi
NEEDNEW=[ "3811" ]

# --- fine configurazione


# --- tabella conversione colori da LDD a Bricklink

BASE=[ # LDD id, LDD name, BL id, BL name, notes
  [1, "White", 1, "White"],
  [2, "Grey", 9, "Light Gray"],
  [3, "Light Yellow", 33, "Light Yellow", "", "", "Discontinued 2004"],
  [5, "Brick Yellow", 2, "Tan"],
  [6, "Light Green", 38, "Light Green"],
  [9, "Light Reddish Violet", 23, "Pink", "", "", "Discontinued 2004"],
  [11, "Pastel Blue", 72, "Maersk Blue"],
  [12, "Light Orange Brown", 29, "Earth Orange"],
  [18, "Nougat", 28, "Flesh"],
  [20, "Nature", 60, "Milky White"],
  [21, "Bright Red", 5, "Red", "'One of at least two colors Bricklink calls ''Red'''", "", "Still in use/2013"],
  [22, "Medium Reddish Violet", 47, "Dark Pink", "'One of at least two colors Bricklink calls ''Dark Pink'''", "", "Discontinued 2004"],
  [23, "Bright Blue", 7, "Blue", "'One of at least two colors Bricklink calls ''Blue'''", "", "Still in use/2013"],
  [24, "Bright Yellow", 3, "Yellow", "", "", "Still in use/2013"],
  [25, "Earth Orange", 8, "Brown", "'One of at least two colors Bricklink calls ''Brown'''", "", "Discontinued 2004"],
  [26, "Black", 11, "Black", "", "", "Still in use/2013"],
  [27, "Dark Grey", 10, "Dark Gray", "", "", "Discontinued 2004"],
  [28, "Dark Green", 6, "Green", "'One of at least two colors Bricklink calls ''Green'''", "", "Still in use/2013"],
  [29, "Medium Green", 37, "Medium Green"],
  [37, "Bright Green", 36, "Bright Green", "", "", "Still in use/2013"],
  [38, "Dark Orange", 68, "Dark Orange", "", "", "Still in use/2013"],
  [39, "Light Bluish Violet", 44, "Light Violet"],
  [40, "Transparent", 12, "Trans-Clear", "", "", "Still in use/2013"],
  [41, "Transparent Red", 17, "Trans-Red", "", "", "Still in use/2013"],
  [42, "Transparent Light Blue", 15, "Trans-Light Blue", "'One of at least two colors Bricklink calls ''Trans-Light Blue'''", "", "Still in use/2013"],
  [43, "Transparent Blue", 14, "Trans-Dark Blue", "'One of at least two colors Bricklink calls ''Trans-Dark Blue'''", "", "Still in use/2013"],
  [44, "Transparent Yellow", 19, "Trans-Yellow", "'One of at least two colors Bricklink calls ''Trans-Yellow'''", "", "Still in use/2013"],
  [45, "Light Blue", 62, "Light Blue"],
  [47, "Transparent Fluorescent Reddish Orange", 18, "Trans-Neon Orange", "", "", "Still in use/2013"],
  [48, "Transparent Green", 20, "Trans-Green", "", "", "Still in use/2013"],
  [49, "Transparent Fluorescent Green", 16, "Trans-Neon Green", "", "", "Still in use/2013"],
  [50, "Phosphorescent White", 46, "Glow In Dark Opaque", "'One of at least two colors Bricklink calls ''Glow In Dark Opaque'''", "Introduced 1990", "Discontinued 2006"],
  [100, "Light Red", 26, "Light Salmon"],
  [101, "Medium Red", 25, "Salmon", "'One of at least three colors Bricklink calls ''Salmon'''"],
  [102, "Medium Blue", 42, "Medium Blue", "", "", "Still in use/2013"],
  [103, "Light Grey", 49, "Very Light Gray", "", "", "Discontinued 2004"],
  [104, "Bright Violet", 24, "Purple"],
  [105, "Bright Yellowish Orange", 31, "Medium Orange", "", "", "Discontinued 2004"],
  [106, "Bright Orange", 4, "Orange", "", "", "Still in use/2013"],
  [107, "Bright Bluish Green", 39, "Dark Turquoise"],
  [108, "Earth Yellow", 0],
  [109, "Pc Black Ir", 0],
  [110, "Bright Bluish Violet", 43, "Violet"],
  [111, "Transparent Brown", 13, "Trans-Black", "", "", "Still in use/2013"],
  [112, "Medium Bluish Violet", 73, "Medium Violet", "'One of at least three colors Bricklink calls ''Medium Violet'''"],
  [113, "Transparent Medium Reddish Violet", 50, "Trans-Dark Pink", "'One of at least two colors Bricklink calls ''Trans-Dark Pink'''", "", "Still in use/2013"],
  [114, "Transparent Pink Glitter", 100, "Glitter Trans-Dark Pink"],
  [115, "Medium Yellowish Green", 76, "Medium Lime"],
  [116, "Medium Bluish Green", 40, "Turquoise"],
  [117, "Transparent with Glitter", 101, "Glitter Trans-Clear"],
  [118, "Light Bluish Green", 41, "Aqua"],
  [119, "Bright Yellowish Green", 34, "Lime", "", "", "Still in use/2013"],
  [120, "Light Yellowish Green", 35, "Light Lime"],
  [123, "Bright Reddish Orange", 25, "Salmon", "'One of at least three colors Bricklink calls ''Salmon'''", "", "Discontinued 2004"],
  [124, "Bright Reddish Violet", 71, "Magenta", "", "", "Still in use/2013"],
  [126, "Transparent Bright Bluish Violet", 51, "Trans-Purple", "'One of at least two colors Bricklink calls ''Trans-Purple'''"],
  [127, "Gold", 61, "Pearl Light Gold", "", "", "Discontinued 2006"],
  [128, "Dark Nougat", 29, "Earth Orange", "'One of at least two colors Bricklink calls ''Earth Orange'''"],
  [129, "Transparent Bluish Violet (Glitter)", 102, "Glitter Trans-Purple"],
  [131, "Silver", 66, "Pearl Light Gray", "'One of at least three colors Bricklink calls ''Pearl Light Gray'''", "Introduced 2000; 2007", "Discontinued 2006; 2010"],
  [135, "Sand Blue", 55, "Sand Blue", "", "", "Still in use/2013"],
  [136, "Sand Violet", 54, "Sand Purple"],
  [138, "Sand Yellow", 69, "Dark Tan", "", "", "Still in use/2013"],
  [139, "Copper", 84, "Copper", "'One of at least four colors Bricklink calls ''Copper'''"],
  [140, "Earth Blue", 63, "Dark Blue", "", "", "Still in use/2013"],
  [141, "Earth Green", 80, "Dark Green", "", "", "Still in use/2013"],
  [143, "Transparent Fluorescent Blue", 15, "Trans-Light Blue", "'One of at least two colors Bricklink calls ''Trans-Light Blue'''"],
  [144, "Army Green", 0],
  [145, "Sand Blue Metallic", 78, "Metal Blue", "'One of at least two colors Bricklink calls ''Metal Blue'''"],
  [146, "Sand Violet Metallic", 0],
  [147, "Sand Yellow Metallic", 81, "Flat Dark Gold", "", "", "Discontinued 2006"],
  [148, "Dark Grey Metallic", 77, "Pearl Dark Gray", "'One of at least two colors Bricklink calls ''Pearl Dark Gray'''", "", "Discontinued 2010"],
  [149, "Metallic Black", 11, "Black", "'One of at least two colors Bricklink calls ''Black'''"],
  [150, "Light Gray Metallic", 119, "Pearl Very Light Gray"],
  [151, "Sand Green", 48, "Sand Green", "", "", "Still in use/2013"],
  [153, "Sand Red", 58, "Sand Red"],
  [154, "Dark Red/New Dark Red", 59, "Dark Red"],
  [156, "Transparent Deep Blue", 14, "Trans-Dark Blue", "'One of at least two colors Bricklink calls ''Trans-Dark Blue'''"],
  [157, "Transparent Fluorescent Yellow", 121, "Trans-Neon Yellow", "", "Introduced 2001", "Discontinued 2004"],
  [158, "Transparent Fluorescent Red", 50, "Trans-Dark Pink", "'One of at least two colors Bricklink calls ''Trans-Dark Pink'''", "Introduced 2001", "Discontinued 2004"],
  [168, "Gun Metallic", 0],
  [175, "Transparent Green with Glitter", 0],
  [176, "Red Flip/Flop", 84, "Copper", "'One of at least four colors Bricklink calls ''Copper'''", "Introduced 2001"],
  [178, "Yellow Flip/Flop", 0, "", "", "Introduced 2002"],
  [179, "Silver Flip/Flop", 0, "", "", "Introduced 2002"],
  [182, "Transparent Bright Orange", 98, "Trans-Orange", "'One of at least two colors Bricklink calls ''Trans-Orange'''"],
  [183, "Metallic White", 83, "Pearl White"],
  [184, "Metallic Bright Red", 5, "Red", "'One of at least two colors Bricklink calls ''Red'''", "Introduced 2003"],
  [185, "Metallic Bright Blue", 7, "Blue", "'One of at least two colors Bricklink calls ''Blue'''", "Introduced 2003"],
  [186, "Metallic Dark Green", 6, "Green", "'One of at least two colors Bricklink calls ''Green'''", "Introduced 2003"],
  [187, "Metallic Earth Orange", 8, "Brown", "'One of at least two colors Bricklink calls ''Brown'''", "Introduced 2003"],
  [189, "Reddish Gold", 84, "Copper", "'One of at least four colors Bricklink calls ''Copper'''", "Introduced 2003", "Discontinued 2006"],
  [191, "Flame Yellowish Orange", 110, "Bright Light Orange", "", "Introduced 2004", "Still in use/2013"],
  [192, "Reddish Brown", 88, "Reddish Brown", "", "Introduced 2004", "Still in use/2013"],
  [193, "Flame Reddish Orange", 25, "Salmon", "'One of at least three colors Bricklink calls ''Salmon'''", "Introduced 2004"],
  [194, "Medium Stone Grey", 86, "Light Bluish Gray", "", "Introduced 2004", "Still in use/2013"],
  [195, "Royal Blue", 97, "Blue-Violet", "", "Introduced 2004"],
  [196, "Dark Royal Blue", 109, "Dark Blue-Violet", "", "Introduced 2004"],
  [198, "Bright Reddish Lilac", 93, "Light Purple"],
  [199, "Dark Stone Grey", 85, "Dark Bluish Gray", "", "Introduced 2004", "Still in use/2013"],
  [200, "Lemon Metallic", 70, "Metallic Green", "'One of at least two colors Bricklink calls ''Metallic Green'', but the only one with its own Material ID'", "Introduced 2003"],
  [208, "Light Stone Grey", 99, "Very Light Bluish Gray", "", "Introduced 2004", "Discontinued 2013"],
  [212, "Light Royal Blue", 105, "Bright Light Blue", "", "", "Still in use/2013"],
  [217, "Brown", 91, "Dark Flesh", "", "Introduced 2004", "Discontinued 2006"],
  [218, "Reddish Lilac", 73, "Medium Violet", "'One of at least three colors Bricklink calls ''Medium Violet'''"],
  [219, "Lilac", 73, "Medium Violet", "'One of at least three colors Bricklink calls ''Medium Violet'''"],
  [221, "Bright Purple", 47, "Dark Pink", "'One of at least two colors Bricklink calls ''Dark Pink'''", "Introduced 2004", "Still in use/2013"],
  [222, "Light Purple", 104, "Bright Pink", "'One of at least two colors Bricklink calls ''Bright Pink'''", "Introduced 2004", "Still in use/2013"],
  [225, "Warm Yellowish Orange", 32, "Light Orange", "'One of at least two colors Bricklink calls ''Light Orange'''"],
  [226, "Cool Yellow", 103, "Bright Light Yellow", "", "Introduced 2004", "Still in use/2013"],
  [227, "Transparent Bright Yellowish Green", 108, "Trans-Bright Green", "'One of at least three colors Bricklink calls ''Trans-Bright Green'''"],
  [229, "Transparent Light Bluish Green", 113, "Trans-Very Lt Blue"],
  [230, "Transparent Bright Purple", 107, "Trans-Pink"],
  [231, "Transparent Flame Yellowish Orange", 98, "Trans-Orange", "'One of at least two colors Bricklink calls ''Trans-Orange'''"],
  [232, "Dove Blue", 87, "Sky Blue"],
  [234, "Transparent Fire Yellow", 19, "Trans-Yellow", "'One of at least two colors Bricklink calls ''Trans-Yellow'''"],
  [236, "Transparent Bright Reddish Lilac", 51, "Trans-Purple", "'One of at least two colors Bricklink calls ''Trans-Purple'''"],
  [268, "Medium Lilac", 89, "Dark Purple"],
  [283, "Light Nougat", 90, "Light Flesh"],
  [284, "Transparent Reddish Lilac", 114, "Trans-Light Purple"],
  [285, "Transparent Light Green", 108, "Trans-Bright Green", "'One of at least three colors Bricklink calls ''Trans-Bright Green'''"],
  [293, "Transparent Light Royal Blue", 74, "Trans-Medium Blue", "'One of at least two colors Bricklink calls ''Trans-Medium Blue'''"],
  [294, "Phosphorescent Green", 46, "Glow In Dark Opaque", "'One of at least two colors Bricklink calls ''Glow In Dark Opaque'''", "Introduced 2006", "Discontinued 2012"],
  [295, "Flamingo Pink", 104, "Bright Pink", "'One of at least two colors Bricklink calls ''Bright Pink'''"],
  [296, "Cool Silver", 66, "Pearl Light Gray", "'One of at least three colors Bricklink calls ''Pearl Light Gray'''", "Introduced 2006", "Discontinued 2007"],
  [297, "Warm Gold", 115, "Pearl Gold", "'One of at least three colors Bricklink calls ''Pearl Gold'''", "Introduced 2006", "Still in use/2013"],
  [298, "Cool Silver, Drum Lacquered", 67, "Metallic Silver", "'One of at least three colors Bricklink calls ''Metallic Silver'''", "Introduced 2006", "Still in use/2013"],
  [299, "Warm Gold, Drum Lacquered", 65, "Metallic Gold", "", "Introduced 2006", "Still in use/2013"],
  [300, "Copper, Drum Lacquered", 84, "Copper", "'One of at least four colors Bricklink calls ''Copper'''", "Introduced 2006", "Still in use/2013"],
  [304, "Cool Silver, Diffuse", 111, "Speckle Black-Silver", "", "Introduced 2006"],
  [306, "Copper, Diffuse", 116, "Speckle Black-Copper", "", "Introduced 2006"],
  [308, "Dark Brown", 120, "Dark Brown", "", "Introduced 2008", "Still in use/2013"],
  [309, "Metalized Silver", 22, "Chrome Silver", "", "", "Still in use/2013"],
  [310, "Metalized Gold", 21, "Chrome Gold", "", "", "Still in use/2013"],
  [311, "Transparent Bright Green", 108, "Trans-Bright Green", "'One of at least three colors Bricklink calls ''Trans-Bright Green'''", "Introduced 2010", "Still in use/2013"],
  [312, "Medium Nougat", 150, "Medium Dark Flesh", "", "Introduced 2010", "Still in use/2013"],
  [315, "Silver Metallic", 66, "Pearl Light Gray", "'One of at least three colors Bricklink calls ''Pearl Light Gray'''", "Introduced 2010", "Still in use/2013"],
  [316, "Titanium Metallic", 77, "Pearl Dark Gray", "'One of at least two colors Bricklink calls ''Pearl Dark Gray'''", "Introduced 2010", "Still in use/2013"],
  [321, "Dark Azur", 153, "Dark Azure", "", "Introduced 2011", "Still in use/2013"],
  [322, "Medium Azur", 156, "Medium Azure", "", "Introduced 2012", "Still in use/2013"],
  [323, "Aqua", 152, "Light Aqua", "", "Introduced 2011", "Still in use/2013"],
  [324, "Medium Lavender", 157, "Medium Lavender", "", "Introduced 2012", "Still in use/2013"],
  [325, "Lavender", 154, "Lavender", "", "Introduced 2011", "Still in use/2013"],
  [326, "Spring Yellowish Green", 158, "Yellowish Green", "", "Introduced 2012", "Still in use/2013"],
  [329, "White Glow", 159, "Glow In Dark White", "", "Introduced 2012", "Still in use/2013"],
  [330, "Olive Green", 155, "Olive Green", "", "Introduced 2012", "Still in use/2013"]
]

def colore lddmaterial, codicepezzo
  BASE.each do |r|
    next if r.first != lddmaterial
    rv = r[2]
    fail "pezzo #{codicepezzo}: colore inesistente su bricklink! #{lddmaterial}: #{r[1]}"  if rv == 0
    return rv
  end
  fail "pezzo #{codicepezzo}: colore non trovato: #{lddmaterial}"
end      



# --- main: si aspetta su stdin l'export .xhtml di libreoffice calc:

tot, lot = 0,0

puts "<INVENTORY>"
while a = gets
  b = a.split('=')
  next unless b
  next unless b.first.strip == '--><div style'
  c = a.gsub('</p>', '')
  d = c.split('</td>')
  id = d[1].split('>').last

  next if IGNORELIST.include?(id)

  id = SUBSTLIST[id]  if SUBSTLIST[id]

  col = d[2].split('>').last.split.first

  next if col == IGNORECOLOR
  col = "194"  if id=='23443'   # correzione bislacco bug di LDD

  srv = d[4].split('>').last.to_i
  next if srv == 0
  cbl = colore col.to_i, id.to_i

  cond = CONDDEFAULT
  cond = "U"  if NEEDUSED.include?(id)
  cond = "N"  if NEEDNEW.include?(id)

  #puts "part[#{id}] col[#{col} -> #{cbl}] srv[#{srv}]"

  puts "<ITEM>
<ITEMTYPE>P</ITEMTYPE>
<ITEMID>#{id}</ITEMID>
<COLOR>#{cbl}</COLOR>
<MINQTY>#{srv}</MINQTY>
<CONDITION>#{cond}</CONDITION>
<NOTIFY>N</NOTIFY>
</ITEM>"

  lot += 1
  tot += srv
end
puts "</INVENTORY>"

STDERR.puts "#{tot} pezzi in #{lot} lotti"
