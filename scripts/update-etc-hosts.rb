#!/usr/bin/env ruby

# -- allow these hosts despite what the blacklist says:
#
whitelist = %w{ www.rt.com sputniknews.com }

# -- where to get the latest updated blacklist:
# ("fakenews" websites, "gambling" websites, "porn" websites)
#
url = "https://raw.githubusercontent.com/StevenBlack/hosts/master/alternates/fakenews-gambling-porn/hosts"

# -- hostname of this machine:
#
h = `hostname`.chomp

# -- default /etc/hosts: define addresses for localhost, ipv6, etc;
# also define your own LAN (local area network) preferred addresses
# also define your own blacklist (annoying websites/servers):
#
h = "127.0.0.1       localhost localhost.localdomain
127.0.1.1       #{h}
::1             ip6-localhost ip6-loopback localhost6.localdomain6 localhost6
fe00::0         ip6-localnet
ff00::0         ip6-mcastprefix
ff02::1         ip6-allnodes
ff02::2         ip6-allrouters

# -- LAN -- last update: #{Time.now}
192.168.1.1     router

# -- black-list
0.0.0.0         beeketing.com
0.0.0.0         ekansovi.com
0.0.0.0         detectportal.firefox.com
0.0.0.0         appboy.com
0.0.0.0         dev.appboy.com
0.0.0.0         graph.facebook.com
0.0.0.0         yip.su
0.0.0.0         2no.co
0.0.0.0         easylist.to
0.0.0.0         coinhive.com
0.0.0.0         productsearch.ubuntu.com
0.0.0.0         da.feedsportal.com
0.0.0.0         vine.co
0.0.0.0         zeus.pirchio.com

# -- #{url}
"

# -- main ---------------------------------------------------------------------

# fetch the latest blacklist:
#
s = `curl #{url}`
fail "file surprisingly short (#{s.size} bytes: #{url})"  if s.size<999999

# safe to force to utf-8 since it's domains and comments:
#
s.force_encoding("UTF-8")
s = s.split(/\n/)

# apply our whitelist (delete from blacklist):
#
whitelist.each do |w|
        w = " #{w}"
        s.delete_if { |r| r.end_with?(w) }
end

# write the updated /etc/hosts:
#
h = h + s.join("\n")
open('/etc/hosts','w').print h
File.chmod(0o644, '/etc/hosts')

# print statistics (count non-empty/non-comment lines):
#
z = h.size
s.delete_if { |r|  r.size==0 || r[0]=='#' }
puts "!--#{s.size} hosts, #{z} bytes"

# ---
