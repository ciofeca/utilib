#!/usr/bin/env bash

# --- configuration:
#
# assuming this webcam file changes at least every ~60 seconds:
#
CAM=https://www.iu1fig.com/wxdata/losio/camtower.jpg

# we will try to extract a new image every 180 seconds,
# except when a download goes wrong
#
EVR=180
WAI=20

# --- main:
#
x=$0
while true
do
	lastx=$x
	while true
	do
		x=`date +%d%H%M%S`.jpg
		while true
		do
			if curl --silent --output $x $CAM
			then
				break
			fi
		done

		if [ ! -s $x ]
		then
			# download problems or zero-lenght
			rm --force $x
			sleep $WAI
			continue
		fi
	
		if cmp --quiet $lastx $x
		then
			# same as previous file
			rm --force $x
			sleep $[ $WAI * 3 ]
			continue
		fi

		break
	done

	echo ---OK: $x
	sleep $EVR
done
