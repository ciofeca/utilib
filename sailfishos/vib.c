//
//  vib.c
//
//  description:
//  - command-line utility to activate vibration on SailfishOS
//  - does its thing by dynamically loading a library
//
//  use-case:
//  - lose your SailfishOS cellphone in your room
//  - access it via ssh
//  - turn on vibration
//  - found!
//
//  warning:
//  - SailfishOS may stop the vibration after about half a second
//  if the phone was on energy saving mode or set on a
//  totally silent / no vibration profile
//
//  crosscompile it with:
//  - arm-linux-gnueabihf-gcc vib.c -o vib -ldl
//
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <dlfcn.h>

#define LIBVIBRATOR "/usr/lib/libvibrator.so.1"
#define LIBNGFVIBRA "/usr/lib/ngf/libngfd_droid-vibrator.so"
#define LIBGLIB     "libglib-2.0.so.0"
#define SYSVIBRA    "/sys/devices/virtual/timed_output/vibrator/enable"

#define MAXTIME     3000  // limit vibration commands to 3 seconds

// error message and early exit:
//
#define fail_if(x, y...) if(x) { fprintf(stderr, "!--" y); exit(1); }

// get a pointer to a library function:
//
#define fetch_func(sym, name, args) \
	int (* sym )( args ) = (int(*)( args ))dlsym(lib, name); \
	fail_if(! sym, "library does not support '%s'\n", name)


int main(int argc, char** argv) {
  fail_if(argc != 2 || !strcmp(argv[1], "-h") || !strcmp(argv[1], "--help"),
      "usage: %s milliseconds\n", argv[0]);

  int msec = atoi(argv[1]);
  int debug = strcmp(argv[1], "debug") == 0;
  if(debug) {
    msec = 1000;
  }
  fail_if(msec < 0 || msec > MAXTIME, "invalid number of milliseconds\n");

  // try the sys interface first:
  //
  if(debug) fprintf(stderr, "!--now trying interface: %s\n", SYSVIBRA);
  FILE* fp = fopen(SYSVIBRA, "w");
  if(fp != NULL)
  {
    fprintf(fp, "%d\n", msec);
    fclose(fp);
    return 0;
  }

  // try the canonical libvibrator:
  //
  void *lib = NULL;
  fp = fopen(LIBVIBRATOR, "r");
  if(fp != NULL)
  {
    if(debug) fprintf(stderr, "!--now trying library: %s\n", LIBVIBRATOR);
    fclose(fp);

    lib = dlopen(LIBVIBRATOR, RTLD_LAZY);
    if(lib == NULL) {
      fprintf(stderr, "!--cannot use %s: %s\n", LIBVIBRATOR, dlerror());
      // fallback to next library
    }
    else
    {
      fetch_func(vinit, "hybris_vibrator_initialize", void);
      fetch_func(vexists, "vibrator_exists", void);
      fetch_func(von, "vibrator_on", int);
      fetch_func(voff, "vibrator_off", void);

      (*vinit)();
      fail_if(!(*vexists)(), "error: no vibration feature exists in this system\n");

      if(msec > 0) {
        (*von)(msec);
      } else {
        (*voff)();
      }

      return 0;
    }
  }

  // try the alternate library:
  //
  fp = fopen(LIBNGFVIBRA, "r");
  if(fp != NULL)
  {
    if(debug) fprintf(stderr, "!--now trying library: %s\n", LIBNGFVIBRA);
    fclose(fp);

    // glib 2.0 is required first:
    //
    void *lib = dlopen(LIBGLIB, RTLD_LAZY);
    fail_if(lib == NULL, "error: %s required but unavailable\n", LIBGLIB);

    fetch_func(gclose, "g_free", void);

    lib = dlopen(LIBNGFVIBRA, RTLD_NOW);
    if(lib == NULL)
    {
      fprintf(stderr, "%s\n", dlerror());
      // fallback to next library
    }
    else
    {
      fetch_func(vopen, "h_vibrator_open", void);
      fetch_func(vclose, "h_vibrator_close", void);
      fetch_func(von, "h_vibrator_on", int);
      fetch_func(voff, "h_vibrator_off", void);

      fail_if(!(*vopen)(), "error: cannot activate vibration in this system\n");

      if(msec > 0) {
        (*von)(msec);
      } else {
        (*voff)();
      }

      (*vclose)();
      return 0;
    }
  }

  fail_if(lib == NULL, "error: no suitable vibration library found\n");
  return 0;
}

// ---
