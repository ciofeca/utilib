uu# SailfishOS VNC server

*Problem:* accessing a SailfishOS desktop sporting a broken display.

*Solution:* VNC server running on the SailfishOS device, so that a desktop VNC client on the PC can access the SailfishOS screen (including touch/drag gestures).

*Sources:* compile, build and install *[lipstick2vnc](https://github.com/mer-qa/lipstick2vnc)* and *[libvncserver](https://github.com/mer-qa/libvncserver)*.

*Tested on:* SailfishOS 4.1 Kvarken freshly installed on an Xperia XA2.

*WARNING:* apparently it stopped working after SailfishOS 4.6 update. The reported error (in the systemd journal) is "lipstick2vnc[...]: [C] unknown:0 - Failed to create display (No such file or directory)".

# Installation (pre-compiled packages)

*Disclaimer:* no warranty expressed or implied blah blah blah about precompiled packages; if you don't want to trust them, build them by yourself using the above mentioned sources (see below).

*Note:* these are 32-bit packages, I don't know if/how they will work on 64-bit native SailfishOS devices (Xperia 10 II, etc).

*How:* send the two RPM package files *([lipstick2vnc](./lipstick2vnc-0.1-1.armv7hl.rpm)* and *[libvncserver](./libvncserver-0.9.10-1.armv7hl.rpm))* to your SailfishOS device and then, via command-line:

    devel-su rpm -Uvh lipstick2vnc-*.rpm libvncserver-*.rpm 

*Quick test:* connect your SailfishOS device via USB and poke its port 5900 to get the usual *RFB 003-008* prompt (works on wifi as well, but *lipstick2vnc* won't allow farther than that).

*Manual restart:* after updating SailfishOS, you may need to manually re-enable the socket port 5900:

    devel-su systemctl enable vnc.socket

To uninstall:

    devel-su rpm -e lipstick2vnc libvncserver

# Security

By default *lipstickvnc* only works on USB networking, you don't want nearby wifi devices (or cellular network users) to snoop around your cellphone desktop.

Also, you don't want absurdly slow display updates (you will get a decent speed using 8-bit client mode over USB instead of native/32 bit).

# Build it yourself

Have latest SailfishOS SDK installed (it will have you choose a development directory, let's say */home/ciofeca/projects*, unpack the *libvncserver* and *lipstick2vnc* there).

Start the virtualbox'ed build engine, wait for it to boot (~30 seconds) and log in:

    VBoxHeadless -s "Sailfish OS Build Engine" &
    ssh -p 2222 -i /opt/SailfishOS/vmshare/ssh/private_keys/engine/mersdk mersdk@localhost

*"I'm in".* Inside the build engine, get the current target version - say, armv7hl (32 bit):

    cd /srv/mer/targets
    TARGET=`echo SailfishOS-*-armv7hl`
    echo $TARGET

Go build (yes, the virtualbox build engine mirrors the */home/* from the host computer):

    cd /home/ciofeca/projects/libvncserver
    mb2 -t $TARGET -s ./rpm/libvncserver.spec build

It will automatically download dependencies if needed. Ignore warnings. Do the same for *lipstick2vnc:*

    cd /home/ciofeca/projects/lipstick2vnc
    mb2 -t $TARGET -s ./rpm/lipstick2vnc.spec build

The relevant RPM files will appear in the respective *RPMS* directories as usual. You can then shut off the virtualbox build engine:

    sudo poweroff

# Real world usage

![broken XA2 display vs PC desktop VNC client](example.jpg)
