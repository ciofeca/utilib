#!/usr/bin/env python3

# since SailfishX 3.2 didn't sport a stock "sqlite3" executable,
# I had to write this one on the fly to query/modify sqlite3 files
# -- don't forget to quote command-line arguments!
#
# syntax:
#    sqlite3.py databasefile.sqlite "sql query..."
#
# example:
#    bin/sqlite3.py ~/.local/share/org.sailfishos/sailfish-browser/sailfish-browser.sqlite "select url from link where link_id in (select tab_history_id from tab)"

import sqlite3,sys
x = sqlite3.connect(sys.argv[1])
for r in x.execute(" ".join(sys.argv[2:])):
    print("\t".join(map(str,r)))
x.commit()
