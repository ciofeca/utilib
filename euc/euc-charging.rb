#!/usr/bin/env ruby

# --- configuration
#
cells = 20          # number of cells in a group (example: 20 for a 20s4p battery)
#
voltagedrop = 2.1   # if we get this much voltage drop, assume charger got disconnected
#
# --- configuration ends here

class Time
  def diff tnow
    seconds = (tnow - self).to_i
    "%3d'%02d\"" % [ seconds / 60, seconds % 60 ]
  end
end


# --- main
#
eucfname = if ARGV.size == 1  # arguments: a filename, or standard input, or no args for latest file in $HOME/euc
  ARGV.first
elsif !STDIN.tty?
  '<stdin>'
else
  files = ENV['HOME'] + '/euc/EUC data 20*'
  Dir[files].last
end

now, volt, lastv, t, firstt, dist, tick, lasttick = nil
maxv, maxnow, maxvolt, maxvoltdiff, maxcell = nil
fp = eucfname == '<stdin>' ? STDIN : open(eucfname)
puts "!-- #{eucfname.inspect}"

while a = fp.gets
  datetime,duration,duration_riding,distance,distance_total,speed,speed_avg,speed_avg_riding,speed_max,speed_limit,acceleration,voltage,current,current_phase,power,battery,temp,temp_motor,temp_batt,safety_margin,cpu_load,tilt,roll,fan,alert,alarm,gps_datetime,gps_duration,gps_duration_riding,gps_distance,gps_lat,gps_lon,gps_speed,gps_speed_avg,gps_speed_avg_riding,gps_speed_max,gps_alt,gps_bearing,gps_acc,hr,extra = a.chomp.split ','

  next  if datetime == 'datetime'

  unless dist
    if distance.to_f != 0.0
      dist = true
      puts '!-- riding detected (non-zero distance)'
    end
  end

  v = voltage.to_f
  lastv ||= v
  maxv ||= v
  break  if (lastv - v) > voltagedrop

  volt = "%5.2fV" % [ v ]
  cell = "%5.3fV" % [ v / cells ]
  voltdiff = if v - lastv >= 0
               vdiff = "%.2f" % [ v - lastv ]
               "%5s" % [ "+#{vdiff}" ]
             else
               "%5.2f" % [ v - lastv ]
             end

  now = datetime.split(/[T\.]/)[1]
  t = Time.local(*(datetime.split(/[T:\.\-]/)[0..5]))
  firstt ||= t
  warn = if alarm == '0'
           ''
         else
           "alarm:#{alarm}"
         end

  if v > maxv
    maxnow, maxvolt, maxvoltdiff, maxcell, maxtemp, maxv = now, volt, voltdiff, cell, temp, v
  end

  if battery != tick
    if tick
      puts "!-- #{now}  #{lasttick.diff t}  #{volt}  #{voltdiff}  #{cell}  #{temp}°C  #{battery}%  #{warn}"
    end

    lastv = v
    lasttick = t
    tick = battery
  end
end

puts "!-- #{now}  #{lasttick.diff t}  #{volt}  #{voltdiff}  #{cell}  #{temp}°C  #{battery}%  #{warn}"
puts "!-- #{now}  #{firstt.diff t}"
puts "!-- #{maxnow}   max V:  #{maxvolt}  #{maxvoltdiff}  #{maxcell}  #{maxtemp}°C"

