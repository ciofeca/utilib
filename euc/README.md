# Electric unicycle (EUC) utilities & scripts

A few scripts I wrote to manage *euc.world* log files. Note:

1. using [euc.world](https://euc.world) app for electric unicycle riding data logging (Android OS)

- log file format is a 40+ fields CSV, first line describing fields names (currently supporting *euc.world* version 2.x)
- *euc.world* log files have to be enabled (main menu / Data Logging / Start Logging Automatically)
- log files are usually saved on the internal shared storage of the cellphone, in the *Downloads/EUC World* directory

2. using [GPStuff](https://gitlab.com/ciofeca/harbour-gpstuff) app for supplementary GPS time/position/speed data logging (SailfishOS)

3. using [GPXSee](https://www.gpxsee.org) software for logged positions map view (Windows, Linux, Mac)

- only tested on desktop Linux
- Windows users should enable GL rendering in the Settings if large files are too slow to plot
- below example: using Settings / Options / Points / Waypoints Size 2

4. using [GNUplot](http://www.gnuplot.info) software for graphing

5. using [LibreOffice Calc](https://www.libreoffice.org) for spreadsheet

- *LibreOffice* supports the FODS file format (flat-XML opendocument spreadsheet), easy to generate
- while *LibreOffice* can parse regular CSV files, a dozen important columns with good formatting is way better than the fully unformatted 40+ columns of the *euc.world* log file

## euc2csv-gpxsee.rb

Given an *euc.world* log file, extract GPS positions to load in *GPXSee.* The simplest input format accepted by *GPXSee* is a three fields CSV waypoint list (longitude, latitude, name). No configuration needed. Usage example:

    ruby euc2csv-gpxsee.rb < "EUC data 2023-05-23 133844.csv" > today.csv
    gpxsee today.csv

![gpxsee map example](./euc2csv-gpxsee.jpg)

Another example: cumulate a month of electric unicycle riding log files:

    cat "EUC data 2023-05-"*.csv | ruby euc2csv-gpxsee.rb > /tmp/may.csv
    gpxsee /tmp/may.csv

## plottardo.rb

Given an *euc.world* log file, build *gnuplot* graphs for speeds, status (voltage, current, battery, temperature) and position. Also works with *gpstuff* output files, limited to speeds and position graphing. Will output PNG files. Configuration is at the beginning of the script. Usage example:

    ruby plottardo.rb < "EUC data 2023-05-23 133844.csv"
    gimp /tmp/status.png /tmp/positions.png /tmp/speeds.png

Left scale: voltage (usually 84 down to 64 volts), battery (100% to 0%, as reported by the EUC), temperature (+0°C to +95°C); right scale: current (on my EUC it's usually up to 60A riding, down to -30A regenerative braking).

![plottardo status example](./plottardo.png)

## euc.rb

Given an *euc.world* log file, print some statistics and build a tweet message summary (less than 240 bytes); also create a spreadsheet in *.fods* format suitable for *LibreOffice Calc.* Configuration is at the beginning of the script; the editable template *.fods* file has to be in the same directory of the *euc.rb* script. The script either accepts a pathfilename argument, or goes for the latest log file found in *$HOME/euc* directory. Usage example:

    ruby euc.rb
    localc /tmp/euc-20230528-101407.fods

Note that the first time you load the *.fods* file, you have to manually recalculate the sheet by pressing *ctrl-alt-F9* to update the formula cells values and conditional formatting. The sheet can be then saved in regular *.ods* format. (The below example shows the odometer updating every ~10 meters because that's how my EUC emits distance values; also note I set a speed alarm at 48 km/h)

![euc script example](./euc.jpg)

## euc-charging.rb

Given an *euc.world* log file of a battery charging session (no riding, only relevant for voltages and temperatures), show battery percentage/voltage variations and time required to acquire "1% more" charge. Columns shown:

- timestamp
- how much time was needed to get +1% battery charge
- currently reported voltage at the +1% tick time
- voltage difference since previous tick (voltage is actually floating, so this may occasionally show as negative)
- estimated mean cell voltage
- reported system temperature (battery temperature is only available on specific models)
- reported battery level (percent)

The two extra lines at the end report the log file overall timespan and the maximum voltage (may happen way before the charging+balancing is complete). Example output, V11 with stock XVE charger:

![euc script example](./euc-charging.png)
