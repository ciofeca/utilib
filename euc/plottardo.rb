#!/usr/bin/env ruby

# --- configuration ---

# gnuplot executable:
#
GNUPLOT='gnuplot'

# gnuplot configuration for the .PNG output files
# - better use 1280 if you plan to send the PNG files over whatsapp/telegram in their "compressed" mode
#
WIDTH=1280
HEIGHT=960
FILE1='/tmp/speeds.png'
FILE2='/tmp/positions.png'
FILE3='/tmp/status.png'
PNG1="set terminal png enhanced large size #{WIDTH},#{HEIGHT}\nset output '#{FILE1}'"
PNG2="set terminal png enhanced large size #{WIDTH},#{HEIGHT}\nset output '#{FILE2}'"
PNG3="set terminal png enhanced large size #{WIDTH},#{HEIGHT}\nset output '#{FILE3}'"

# speeds graph: colors and thresholds
#
FACTOR=1.1    # 10% difference in speed will switch the point shape
SLO='5e0000'  # rgb color when slowing down
TYP='11ff11'  # rgb color when no significant acceleration/deceleration
ACC='3333ff'  # rgb color when accelerating 

# positions graph: thresholds and vertical scale parameters
#
SPD1=20.0     # "black" if below SPD1, "blue" if between SPD1 and SPD2
SPD2=30.0     # "red" if above SPD2
SPDMIN=-0.2   # -0.2 to have the points drawn above the graph line
SPDMAX=56.7   # km/h, rounded enough to accommodate small errors

# status graph: colors and thresholds
#
COL='006a01'  # rgb color for amperage
VOLMIN=60.0   # minimum voltage
VOLMAX=88.0   # maximum voltage
AMPMIN=-30.5  # lowest plottable value for braking power
AMPMAX=60.5   # highest plottable value for acceleration power

# extended time format allowing for "past midnight" events:
#
TIMEFMT='%Y-%m-%d.%H:%M:%S'

# --- configuration end ---

if ARGV.size > 0
  outfiles = "#{FILE1} #{FILE2} #{FILE3}"
  STDERR.puts "!--usage: #$0 < { gps-[date]-[time].txt | EUC*[date time].csv } && ls -al #{outfiles}"
  exit 1
end
require 'date'

# emit a percent string out of a couple integers:
#
class Integer
  def perc total
    "%.1f%%" % [ (self.to_f / total.to_f) * 100.0 ]
  end
end

# convert "Unix milliseconds" string to a time or a date:
#
class String
  def to_ext_t
    Time.at(to_i/1000).strftime TIMEFMT
  end

  def to_t
    Time.at(to_i/1000).strftime '%H:%M:%S'
  end

  def to_d
    Time.at(to_i/1000).strftime '%A %d %B %Y'
  end
end

# initialize all fields, including unused ones
#
datetime,duration,duration_riding,distance,distance_total,speed,speed_avg,speed_avg_riding,speed_max,speed_limit,acceleration,voltage,current,current_phase,power,battery,temp,temp_motor,temp_batt,safety_margin,cpu_load,tilt,roll,fan,alert,alarm,gps_datetime,gps_duration,gps_duration_riding,gps_distance,gps_lat,gps_lon,gps_speed,gps_speed_avg,gps_speed_avg_riding,gps_speed_max,gps_alt,gps_bearing,gps_acc,hr,extra = nil

# collect records, one per line (tab-separated files):
#   gpstuff: timestamp in "Unix milliseconds", latitude, longitude, speed in km/hr, etc
#   euc.world: datetime, duration, etc
#
versionflag, spdmax, r, bat, t, t0, t1, msec, acceleration = false, 0.0, [], []
while a = STDIN.gets

  if r.empty? && a.split(',').first == 'datetime'  # is this an euc.world log file?
    versionflag = a.include? 'acceleration'        # 2024 releases added acceleration field

    while a = STDIN.gets
      unless versionflag
        datetime,duration,duration_riding,distance,distance_total,speed,speed_avg,speed_avg_riding,speed_max,speed_limit,voltage,current,current_phase,power,battery,temp,temp_motor,temp_batt,safety_margin,cpu_load,tilt,roll,fan,alert,alarm,gps_datetime,gps_duration,gps_duration_riding,gps_distance,gps_lat,gps_lon,gps_speed,gps_speed_avg,gps_speed_avg_riding,gps_speed_max,gps_alt,gps_bearing,gps_acc,hr,extra = a.split ','
      else
        datetime,duration,duration_riding,distance,distance_total,speed,speed_avg,speed_avg_riding,speed_max,speed_limit,acceleration,voltage,current,current_phase,power,battery,temp,temp_motor,temp_batt,safety_margin,cpu_load,tilt,roll,fan,alert,alarm,gps_datetime,gps_duration,gps_duration_riding,gps_distance,gps_lat,gps_lon,gps_speed,gps_speed_avg,gps_speed_avg_riding,gps_speed_max,gps_alt,gps_bearing,gps_acc,hr,extra = a.split ','
      end

      next  if datetime == 'datetime'

      begin
        msec = (DateTime.parse(datetime).to_time.to_f * 1000).floor.to_i.to_s
      rescue Date::Error
        next
      end

      t = msec
      t0 ||= t
      t1 = t
      bat << [ msec, battery, temp, voltage, current ]

      # using wheel ground speed instead of gps speed,
      # because the former is more accurate (and always present);
      # GPS data may be empty
      #
      spdmax = speed.to_f  if spdmax < speed.to_f

      record = [ msec, gps_lat, gps_lon, speed, gps_alt, '0', gps_acc, '0.0', gps_bearing, '0' ]
      r << record
    end
    break
  end

  fields = a.split
  t, x, y, v = fields
  t0 ||= t
  t1 = t
  spdmax = v.to_f  if spdmax < v.to_f
  r << fields
end

day = t0.to_d
t0 = t0.to_ext_t
t1 = t1.to_ext_t

# battery & status graph
#
if bat.size > 0
  STDERR.puts "!--building #{FILE3}"

  # left side tics:
  # - a bit wider range than 0-100
  # - encompassing battery (100% to 0%), temperature (0 to 100°C) and voltage (usually 84 to 64 volts)
  #
  # rights side tics:
  # - encompassing ampere output (usually up to 50-60A accelerating, down to about -30A when braking)
  #
  IO.popen(GNUPLOT, 'w') do |fpo|
    fpo.puts PNG3+"
      set xdata time
      set timefmt '#{TIMEFMT}'
      set format x '%H:%M'
      set encoding utf8
      set key top right
      set xrange  [ '#{t0}'   : '#{t1}'   ]
      set yrange  [        -1 : 101       ]
      set y2range [ #{AMPMIN} : #{AMPMAX} ]
      set y2tics nomirror
      set grid
      set label ' #{day}' at '#{t0}', 2
      show y2tics
      show label"

    bats, temps, volts, amps = [], [], [], []
    bat.each do |i|
      t, b, c, v, a = i
      rec = "#{t.to_ext_t}\t"
      bats << rec + b   if b != ''
      temps << rec + c  if c != ''

      if v != ''
        val = (v.to_f - VOLMIN) / (VOLMAX - VOLMIN) * 100.0
        volts << rec + val.to_s
      end

      if a != ''
        val = (a.to_f - AMPMIN) / (AMPMAX - AMPMIN) * 100.0
        amps << rec + val.to_s
      end
    end

    fpo.puts '$volts << OK', volts, 'OK'
    fpo.puts '$amps << OK', amps, 'OK'
    fpo.puts '$bats << OK', bats, 'OK'
    fpo.puts '$temps << OK', temps, 'OK'
    fpo.puts "plot '$volts' using 1:2 title 'voltage (V)'  with dots   lc rgb '0x#{ACC}',  \\
                   '$amps'  using 1:2 title 'current (A)'  with dots   lc rgb '0x#{COL}',  \\
                   '$bats'  using 1:2 title 'battery (%)'  with lines  lc rgb '0x#{SLO}',  \\
                   '$temps' using 1:2 title 'temp (degC)'  with lines  lc rgb '0x#{TYP}'"
  end
end

# fetch minimum and maximum values:
#
fail '!--no GPS positions'  if r.size == 0

day = r.first.first.to_d
t0 = r.first.first.to_ext_t
t1 = r.last.first.to_ext_t
vmax = r.max_by { |i| i[3].to_f }
vmax = [ vmax.first.to_t, vmax[3], vmax[1], vmax[2] ]

# filter out empty positions, calc min/max latitude/longitude
xmin = r.filter_map { |i| i[2].to_f  if i[2] != '' }.min
xmax = r.filter_map { |i| i[2].to_f  if i[2] != '' }.max
ymin = r.filter_map { |i| i[1].to_f  if i[1] != '' }.min
ymax = r.filter_map { |i| i[1].to_f  if i[1] != '' }.max

labelposition = ymax - (ymax-ymin)*0.02
s0, s1, s2 = 0, 0, 0
last, slo, typ, acc = 0.0, [], [], []

# split speeds into three arrays: "slowing down", "typical", "accelerating",
# then feed gnuplot with clock/speed pairs:
#
STDERR.puts "!--building #{FILE1}"
IO.popen(GNUPLOT, 'w') do |fpo|
  # speeds png will have min/max tics on both left and right side
  #
  fpo.puts PNG1+"
    set xdata time
    set timefmt '#{TIMEFMT}'
    set format x '%H:%M'
    set encoding utf8
    set key top right
    set xrange  [ '#{t0}'   : '#{t1}'   ]
    set yrange  [ #{SPDMIN} : #{spdmax} ]
    set y2range [ #{SPDMIN} : #{spdmax} ]
    set y2tics nomirror
    set grid
    set label ' #{day}' at '#{t0}', #{SPDMAX-1}
    show y2tics
    show label"

  r.each do |i|
    t, x, y, v = i
    rec = "#{t.to_ext_t}\t#{v}"  if t
    rec ||= ""
    spd = v.to_f
    if spd >= SPD2
      s2 += 1
    elsif spd >= SPD1
      s1 += 1
    else
      s0 += 1
    end

    if spd * FACTOR < last
      slo << rec
    elsif last * FACTOR < spd
      acc << rec
    else
      typ << rec
    end

    last = spd
  end

  fpo.puts "$slo << OK", slo, "OK"
  fpo.puts "$typ << OK", typ, "OK"
  fpo.puts "$acc << OK", acc, "OK"
  fpo.puts "plot '$slo' using 1:2 title 'slowing down' with points pointtype 10 lc rgb '0x#{SLO}' pointsize 0.6,\\
                 '$typ' using 1:2 title '       km/hr' with points pointtype 13 lc rgb '0x#{TYP}' pointsize 0.7,\\
                 '$acc' using 1:2 title 'accelerating' with points pointtype 8  lc rgb '0x#{ACC}' pointsize 0.5"
end

# feed gnuplot with positions heat map (sort of),
# then overlay with speeds >= SPD1, then overlay with speeds >= SPD2;
# also unset latitude/longitude tics for privacy concerns
#
STDERR.puts "!--building #{FILE2}"
IO.popen(GNUPLOT, 'w') do |fpo|
  fpo.puts PNG2+"
    unset xtics
    unset ytics
    set encoding utf8
    set key top right
    set xrange [ '#{xmin}' : '#{xmax}' ]
    set yrange [ '#{ymin}' : '#{ymax}' ]
    set label ' #{day}' at #{xmin}, #{labelposition}
    show label
    $map << OK"

  r.each do |i|       # only insert valid GPS positions
    t, x, y, v = i
    fpo.puts "#{x}\t#{y}"  if x != '' && y != ''
  end

  fpo.puts "OK
    $pap << OK"
  r.each do |i|
    t, x, y, v = i
    fpo.puts "#{x}\t#{y}"  if v.to_f >= SPD1 && v.to_f < SPD2
  end

  fpo.puts "OK
    $pam << OK"
  r.each do |i|
    t, x, y, v = i
    fpo.puts "#{x}\t#{y}"  if v.to_f >= SPD2
  end

  fpo.puts "OK
    plot '$map' using 2:1 title '' with points pointtype 2 lc rgb 'black' pointsize 0.3,\\
         '$pap' using 2:1 title '' with points pointtype 2 lc rgb 'blue'  pointsize 0.3,\\
         '$pam' using 2:1 title '' with points pointtype 2 lc rgb 'red'   pointsize 0.3"
end

# debugging/statistics output:
#
STDERR.puts "!--#{r.size} records (#{t0[-8..-1]}..#{t1[-8..-1]}); max speed: #{vmax[1]} (#{vmax[0]})"
STDERR.puts "!--speed: #{s1} > #{SPD1} km/h (#{s1.perc(s0+s1+s2)}) --- #{s2} > #{SPD2} km/h (#{s2.perc(s0+s1+s2)})"
STDERR.puts "!--GPS window: (#{ymin},#{xmin})-(#{ymax},#{xmax})"

# ---
