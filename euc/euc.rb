#!/usr/bin/env ruby

# input: euc.world .CSV log file (command line argument or standard input) and optional tweet arguments
# output: summary, statistics "tweet", and libreoffice .fods spreadsheet


# --- configuration

# first row in the fods spreadsheet relative timestamps column
rtfods = 7

# speed fix (multiplier) in case the measured tire diameter is different than the stock one
speedfix = 1.00

# voltage fix (to be added to the reported voltage) if internal voltmeter is not calibrated
voltfix = 0.0

# reported forward tilt angle (negative: accelerating, positive: braking)
# of the wheel is usually not that meaningful below 2.5 m/s (9.0 km/hr):
tiltspeed = 9.0

# output directory for .fods file
fodsdir = '/tmp'

# map service link (just append longitude,latitude values)
#
MAPSLINK = 'https://www.google.com/maps/search/?api=1&query='
def maps lat, lon
  if lat != '' && lon != ''
    "#{MAPSLINK}#{lat},#{lon}"
  else
    "\t"
  end
end

# to force localization to a different language
#
def localization t
  loc1 = { 'Sun' => 'domenica', 'Mon' => 'lunedì', 'Tue' => 'martedì',
           'Wed' => 'mercoledì', 'Thu' => 'giovedì', 'Fri' => 'venerdì', 'Sat' => 'sabato' }
  loc2 = { 'Jan' => 'gennaio', 'Feb' => 'febbraio', 'Mar' => 'marzo', 'Apr' => 'aprile',
           'May' => 'maggio', 'Jun' => 'giugno', 'Jul' => 'luglio', 'Aug' => 'agosto',
           'Sep' => 'settembre', 'Oct' => 'ottobre', 'Nov' => 'novembre', 'Dec' => 'dicembre' }

  "#{loc1[t.strftime '%a']} #{t.strftime '%e'} #{loc2[t.strftime '%b']}"

  # or just return t.strftime('%a %e %b') to not to explicitly localize weekdays and month names
end

# unique text labels marking for .fods navigation and default values to be set
#
uniquetext = 'temperature'
uniquedate = 'date_place'
uniquedist = 'distance_place'
uniquerecs = 'records_place'

# default arguments to put at the end of the tweet text
#
defaulttags = "#electricUnicycle"
euctag = ''
distdiff = 0.0  # 0 km = wheel was brand new

# since you can have more than one EUC, check the header info
# for the reported model or serial number,
# to initialize the hashtag and the mileage difference
#
def my_euc_config header, euctag, mileagediff
  # I had a brand new Inmotion V10F, then I bought a 2nd hand V11
  # then switched the mainboard of the V10F after more than 10,000 km
  #
  if header == 'eucModel=V10F' || header == 'euc.model=V10F'
    # my first V10F mainboard odometer was 10146.5 km when I switched to a brand new mainboard
    euctag = '#inmotionV10F'
    mileagediff = -10146.5        # my V10F odometer is 10146.5 km behind actual wheel mileage
  elsif header == 'eucModel=V11' || header == 'euc.model=V11'
    # bought my V11 with 2044.4 km (yet ~13.9 were mine before buying it, guess that doesn't count)
    euctag = '#inmotionV11'
    mileagediff = +2044.4         # my V11 odometer is 2044.4 km ahead
  end

  [ euctag, mileagediff ]         # or just use [ '', 0.0 ]
end

# --- configuration ends here


# --- main
#
if ARGV.first && !ARGV.first.empty?          # either a csv file, or the last EUC*.csv file in $HOME/euc
  eucfname = ARGV.first
  tags = "#{ARGV[1..-1].join ' '}"
else
  dir = ENV['HOME'] + '/euc'
  Dir.chdir dir
  eucfname = Dir['EUC*.csv'].sort[-1]        # "EUC data log - #{model} #{serialnumber} - 2024-11-26 220038.csv"
  tags = ''
end
tags = defaulttags  if tags ==''

fp = open eucfname
db = []
fields = fp.gets.chomp.split ','
version = if fields.include? 'acceleration'
            1  # as of 2024, a new field[10] was added to euc.world...
          else
            0
          end

# initiailze all fields, including the unused ones, as of latest version
#
datetime,duration,duration_riding,distance,distance_total,speed,speed_avg,speed_avg_riding,speed_max,speed_limit,acceleration,voltage,current,current_phase,power,battery,temp,temp_motor,temp_batt,safety_margin,cpu_load,tilt,roll,fan,alert,alarm,gps_datetime,gps_duration,gps_duration_riding,gps_distance,gps_lat,gps_lon,gps_speed,gps_speed_avg,gps_speed_avg_riding,gps_speed_max,gps_alt,gps_bearing,gps_acc,hr,extra = nil

# skip to start while fetching the eucModel value
# assume "current <= 1.1 amps" at the beginning means "not yet started riding"
# because less than 1 ampere output usually only happens while carrying/transportation mode
#
while x = fp.gets
  y = x.chomp.split ','
  current = y[11+version].to_f

  # we're searching for "eucModel=...", thus check the last record if 9+ characters
  if euctag == '' && y[-1].size >= 9
    euctag, distdiff = my_euc_config(y[-1], euctag, distdiff)
    next
  end

  # break if no more headers and "started reading"
  break  if euctag != '' && current > 1.1  
end
fail 'uh-oh: no EUC model tag mentioned in the CSV file'  if euctag == ''

# parse .CSV input
#
x0, y0 = 999.999, 999.999
x1, y1 = -999.999, -999.999
e0, e1 = 6000, -999
mintilt, maxtilt = '6000', '-999'
bmax, bmin, voltmin = -999, 999, 999
altmax, lataltmax, lonaltmax, taltmax = 0.0
latE,latW,lonN,lonS,lonE,lonW,latN,latS,tlatE,tlatW,tlonN,tlonS = [ x1, x0, y0, y1 ]
dur, vmax, avg, = 0, '0.0', nil
distmax, distmin, dist = 0.0
max20, max30, max40, max50, streak20, streak30, streak40, streak50, st20, st30, st40, st50 = 0, 0, 0, 0
pmax, amax, pmin, amin, tvmax = 0, 0.0, 0, 0.0, 0
tvvmin, vvamin, latvvmin, lonvvmin, vvspeed, latvmax, lonvmax, avmax, tamax, latamax, lonamax = nil
tamin, latamin, lonamin, ttemp, lattmax, lontmax, distance_total = nil

# main loop
#
while x = fp.gets
  if version == 0
    datetime,duration,duration_riding,distance,distance_total,speed,speed_avg,speed_avg_riding,speed_max,speed_limit,voltage,current,current_phase,power,battery,temp,temp_motor,temp_batt,safety_margin,cpu_load,tilt,roll,fan,alert,alarm,gps_datetime,gps_duration,gps_duration_riding,gps_distance,gps_lat,gps_lon,gps_speed,gps_speed_avg,gps_speed_avg_riding,gps_speed_max,gps_alt,gps_bearing,gps_acc,hr,extra = x.chomp.split ','
  else
    datetime,duration,duration_riding,distance,distance_total,speed,speed_avg,speed_avg_riding,speed_max,speed_limit,acceleration,voltage,current,current_phase,power,battery,temp,temp_motor,temp_batt,safety_margin,cpu_load,tilt,roll,fan,alert,alarm,gps_datetime,gps_duration,gps_duration_riding,gps_distance,gps_lat,gps_lon,gps_speed,gps_speed_avg,gps_speed_avg_riding,gps_speed_max,gps_alt,gps_bearing,gps_acc,hr,extra = x.chomp.split ','
  end

  next  if datetime == 'datetime'              # ignore repeated headers (allow *.CSV concatenation)
  next  if distance_total.to_f <= 0.0          # ignore malformed packets

  dist = distance_total.to_f                   # secure absolute min/max total distance
  distmin = dist  unless distmin
  distmax = dist  unless distmax > dist

  t = datetime.gsub(/[T:\.\+]/, '-').split('-')
  t = Time.local(t[0].to_i, t[1].to_i, t[2].to_i, t[3].to_i, t[4].to_i, (t[5]+'.'+t[6]).to_f)
  d, s, v, c, p, b, e, i = distance.to_f, speed.to_f, voltage.to_f, current.to_f, power.to_i, battery.to_i, temp.to_i, tilt.to_f
  lat, lon = gps_lat, gps_lon

  if b < 0 || b > 100
    puts "!--invalid battery value #{b}% at datetime #{datetime.inspect}"
    next
  end

  if s >= 20.0                    # update 20+ kph streak
    if streak20
      if max20 < t - streak20
        max20 = t - streak20
        st20 = streak20
      end
    else
      streak20 = t
    end
  else
    streak20 = nil
  end

  if s >= 30.0                    # update 30+ kph streak
    if streak30
      if max30 < t - streak30
        max30 = t - streak30
        st30 = streak30
      end
    else
      streak30 = t
    end
  else
    streak30 = nil
  end

  if s >= 40.0                    # update 40+ kph streak
    if streak40
      if max40 < t - streak40
        max40 = t - streak40
        st40 = streak40
      end
    else
      streak40 = t
    end
  else
    streak40 = nil
  end

  if s >= 50.0                    # update 50+ kph streak
    if streak50
      if max50 < t - streak50
        max50 = t - streak50
        st50 = streak50
      end
    else
      streak50 = t
    end
  else
    streak50 = nil
  end

  if gps_alt.to_f > altmax        # save max altitude coordinates
    altmax = gps_alt.to_f
    lataltmax = lat
    lonaltmax = lon
    taltmax = t
  end

  # update battery minimum and maximum voltages, save local statistics
  #
  # note that some EUC's voltmeter depend on timing a capacitor's discharge;
  # this also means that shutting off the EUC will apparently post
  # bizarrely low voltages
  #
  bmax = b  if bmax < b
  if bmin > b
    bmin = b
    abatt = c
    latbmax = lat
    lonbmax = lon
    tbatt = t
    bspeed = speed
  end

  e0 = e    if e0 > e             # new temperature minimum
  if e1 < e                       # new temperature maximum, save local stats
    e1 = e
    lattmax = lat
    lontmax = lon
    ttemp = t
    atemp = c
    stemp = s
  end

  # update tilt values if enough speed
  mintilt = tilt  if mintilt.to_f > i  &&  s >= tiltspeed
  maxtilt = tilt  if maxtilt.to_f < i  &&  s >= tiltspeed

  if vmax.to_f < s                # new speed maximum, save local stats
    vmax = speed
    tvmax = t
    avmax = c
    latvmax = lat
    lonvmax = lon
  end

  avg = speed_avg_riding  if speed_avg_riding != '0.00'  # keep saving latest speed average

  dur = duration_riding.to_i  if duration_riding.to_i > dur

  pmax = p  if pmax < p           # update max power output (watts)
  pmin = p  if pmin > p           # update min power output

  if amax < c                     # update max current (ampere, accelerating), save local stats
    amax = c
    tamax = t
    vamax = speed
    latamax = lat
    lonamax = lon
  end

  if amin > c                     # update min current (ampere, braking), save local stats
    amin = c
    tamin = t
    vamin = speed
    latamin = lat
    lonamin = lon
  end

  if voltmin > v                  # update lowest voltage, save local stats
    voltmin = v
    vvamin = c
    tvvmin = t
    vvspeed = speed
    latvvmin = lat
    lonvvmin = lon
  end

  if gps_lat && gps_lon && gps_lat != '' && gps_lon != ''   # update GPS area, if GPS position available
    lat, lon = gps_lat.to_f, gps_lon.to_f
    x0 = lat  if x0 > lat
    x1 = lat  if x1 < lat
    y0 = lon  if y0 > lon
    y1 = lon  if y1 < lon

    # save the four "most remote" coordinates
    latE,lonE,tlatE = [ lat,lon,t ]  if lat > latE
    latW,lonW,tlatW = [ lat,lon,t ]  if lat < latW
    latS,lonS,tlonS = [ lat,lon,t ]  if lon > lonS
    latN,lonN,tlonN = [ lat,lon,t ]  if lon < lonN
  end

  # add a record to the database
  db << [ t, distance, speed, voltage, current, power, battery, temp, gps_lat, gps_lon, gps_speed, gps_alt, tilt, alarm, safety_margin, speed_max, speed_limit, roll, acceleration ]
end

# fail if no riding data (happens if the log file only covered "battery charging" time)
fail "uh-oh: empty database #{eucfname}"  if db.size == 0

# build the tweet text, usually 235 bytes (slightly less than the allowed 240);
# use unicode symbols ㎞ ℃ because it's funnier
#
timecolon = ':'
dur = sprintf("%d%s%02d%s%02d", dur/3600, timecolon, (dur/60)%60, timecolon, dur%60)
dist = sprintf("%.2f", distmax - distmin)
avg = sprintf("%.2f", avg.to_f)
vmax = sprintf("%.2f", vmax.to_f)
tot = sprintf("%.2f", distmax - distdiff)

vmax.chop!  if vmax[-1] == '0'
tot.chop!   if tot[1] == '0'

max20 = 0  unless max20
max30 = 0  unless max30
max40 = 0  unless max40
max50 = 0  unless max50
m20, m30, m40, m50 = max20.to_i, max30.to_i, max40.to_i, max50.to_i
streak = sprintf("30+ speed streak: %d%s%02d (20+: %d%s%02d)", m30 / 60, timecolon, m30 % 60, m20 / 60, timecolon, m20 % 60)

tweet = "Today's #EUC stats: #{dist} ㎞ in #{dur} (avg riding #{avg} ㎞/h, max #{vmax} ㎞/h), battery #{bmax}→#{bmin}%, temp #{e0}…#{e1}℃, max #{pmax}W @ #{amax}A vs #{pmin}W @ #{amin}A; forward tilt: #{mintilt}° to +#{maxtilt}°; #{streak}; grand total: #{tot} ㎞ #{euctag} #{tags}".strip


# print a summary to stderr and the tweet text
#
STDERR.puts
STDERR.puts "statistics from #{eucfname.inspect}"
STDERR.puts "\tread #{db.size} records"
STDERR.puts "\t\t(#{x0},#{y0})-(#{x1},#{y1})"
STDERR.puts "\tfrom #{db.first.first.strftime '%F %T'} to #{db.last.first.strftime '%T'}"
STDERR.puts "\t\tmax streak 20+ km/h from #{st20.strftime '%T'} #{'%4d' % [m20]} seconds"  if st20
STDERR.puts "\t\tmax streak 30+ km/h from #{st30.strftime '%T'} #{'%4d' % [m30]} seconds"  if st30
STDERR.puts "\t\tmax streak 40+ km/h from #{st40.strftime '%T'} #{'%4d' % [m40]} seconds"  if st40
STDERR.puts "\t\tmax streak 50+ km/h from #{st50.strftime '%T'} #{'%4d' % [m50]} seconds"  if st50
STDERR.puts "\ttext of tweet message is #{tweet.size} bytes:"
STDERR.puts
STDOUT.puts tweet
STDERR.puts
STDERR.puts "top speed\t@#{avmax}A: \t#{maps latvmax, lonvmax}\t#{tvmax.strftime '%T'}  #{vmax} ㎞/h"  if tvmax.class == Time
STDERR.puts "top power\t@#{amax}A: \t#{maps latamax, lonamax}\t#{tamax.strftime '%T'}  #{vamax} ㎞/h"  if tamax.class == Time
STDERR.puts "top brake\t@#{amin}A: \t#{maps latamin, lonamin}\t#{tamin.strftime '%T'}  #{vamin} ㎞/h"  if tamin.class == Time
STDERR.puts "max #{e1}°C \t@#{atemp}A:  \t#{maps lattmax, lontmax}\t#{ttemp.strftime '%T'}  #{stemp} ㎞/h"
STDERR.puts "min #{bmin}%  \t@#{abatt}A:  \t#{maps latbmax, lonbmax}\t#{tbatt.strftime '%T'}  #{bspeed} ㎞/h"
STDERR.puts "min #{voltmin}V\t#{vvamin}A:  \t#{maps latvvmin, lonvvmin}\t#{tvvmin.strftime '%T'}  #{vvspeed} ㎞/h"
STDERR.puts "max altitude:\t#{altmax.ceil}m:    \t#{maps lataltmax, lonaltmax}"  if taltmax
STDERR.puts
STDERR.puts "max North:\t\t\t#{maps latN, lonN }\t#{tlonN.strftime '%T'}"  if tlonN
STDERR.puts "max South:\t\t\t#{maps latS, lonS }\t#{tlonS.strftime '%T'}"  if tlonS
STDERR.puts "max East: \t\t\t#{maps latE, lonE }\t#{tlatE.strftime '%T'}"  if tlatE
STDERR.puts "max West: \t\t\t#{maps latW, lonW }\t#{tlatW.strftime '%T'}"  if tlatW
STDERR.puts

# build .fods spreadsheet (to be loaded by LibreOffice Calc)
#
fname = "#{fodsdir}/#{db.first.first.strftime 'euc-%Y%m%d-%H%M%S.fods'}"
out = open(fname, 'w')
fods = open "#$0.fods"  # open local .fods file
rtf = nil

# explicit Italian localization of the first timestamp because server locale is English:
#
loc = localization db.first.first

# use the local .fods file as a template
#
while a = fods.gets              # copy until header contents
  # convert header labels to actual data
  #
  a[uniquedate] = loc                   if a.include? uniquedate
  a[uniquedist] = "#{tot} km"           if a.include? uniquedist
  a[uniquerecs] = "#{db.size} records"  if a.include? uniquerecs

  out.puts a
  break  if a.include? uniquetext
end
while a = fods.gets              # copy until header ends
  out.puts a
  break  if a.include? '</table:table-row>'
end
while a = fods.gets              # skip until example rows end
  break  if a.include? 'table:number-rows-repeated'
end

# export database to table cells
#
lastt = db.first.first
db.each do |t, distance, speed, voltage, current, power, battery, temp, gps_lat, gps_lon, gps_speed, gps_alt, tilt, alarm, safety, speedmax, speedlimit, roll, acceleration|
  out.puts '    <table:table-row table:style-name="ro1">'

  # forward tilt, just like roll, is barely significant at low speeds;
  # making them text strings will have the min/max ignore it
  #
  of = '%.1f°' % [ tilt.to_f ]
  of = "+#{of}"  unless of.start_with? '-'
  if speed.to_f >= tiltspeed
    out.puts "     <table:table-cell office:value-type=\"float\" office:value=\"#{tilt}\" calcext:value-type=\"float\">"
  else
    out.puts "     <table:table-cell office:value-type=\"string\" office:value=\"#{tilt}\" calcext:value-type=\"string\">"
  end
  out.puts "      <text:p>#{of}</text:p>"
  out.puts '     </table:table-cell>'
  if speed.to_f >= tiltspeed
    out.puts "     <table:table-cell office:value-type=\"float\" office:value=\"#{roll}\" calcext:value-type=\"float\">"
  else
    out.puts "     <table:table-cell office:value-type=\"string\" office:value=\"#{roll}\" calcext:value-type=\"string\">"
  end
  out.puts "      <text:p>#{of}</text:p>"
  out.puts '     </table:table-cell>'

  out.puts "     <table:table-cell office:value-type=\"float\" office:value=\"#{distance}\" calcext:value-type=\"float\">"
  out.puts "      <text:p>#{'%.3f' % [ distance.to_f ]}</text:p>"
  out.puts '     </table:table-cell>'

  out.puts "     <table:table-cell office:value-type=\"float\" office:value=\"#{power}\" calcext:value-type=\"float\">"
  out.puts "      <text:p>#{power}</text:p>"
  out.puts '     </table:table-cell>'

  out.puts "     <table:table-cell office:value-type=\"float\" office:value=\"#{current}\" calcext:value-type=\"float\">"
  out.puts "      <text:p>#{'%.2f' % [ current.to_f ]}</text:p>"
  out.puts '     </table:table-cell>'

  out.puts "     <table:table-cell office:value-type=\"float\" office:value=\"#{voltage.to_f + voltfix}\" calcext:value-type=\"float\">"
  out.puts "      <text:p>#{'%.2f' % [ voltage.to_f + voltfix ]}</text:p>"
  out.puts '     </table:table-cell>'

  # -- temperature, sadly reported as an integer
  #
  out.puts "     <table:table-cell office:value-type=\"float\" office:value=\"#{temp}\" calcext:value-type=\"float\">"
  out.puts "      <text:p>#{temp}°</text:p>"
  out.puts '     </table:table-cell>'

  # -- timestamp
  #
  of = "of:=DATE(#{t.strftime '%Y;%m;%d'})+TIME(#{t.strftime '%H;%M;%S.%L'})"
  of1 = (t.strftime '%FT%T.%L')[0..-2]
  of2 = (t.strftime '%F %T.%L')[0..-3]
  out.puts "     <table:table-cell table:style-name=\"ce20\" table:formula=\"#{of}\" office:value-type=\"date\" office:date-value=\"#{of1}\" calcext:value-type=\"date\">"
  out.puts "      <text:p>#{of2}</text:p>"
  out.puts '     </table:table-cell>'

  # -- elapsed time since last timestamp (difftime)
  # -- watch out for the column name ("H")
  #
  if rtf
    of = "of:=[.H#{rtfods}]-[.H#{rtf}]"
    of1 = Time.at(t-lastt).strftime 'PT00H%HM%MS.%LS'
    of2 = Time.at(t-lastt).strftime '+0.%L'
    out.puts "     <table:table-cell table:formula=\"#{of}\" office:value-type=\"time\" office:time-value=\"#{of1}\" calcext:value-type=\"time\">"
    out.puts "      <text:p>#{of2}</text:p>"
    out.puts "     </table:table-cell>"
  else
    out.puts '     <table:table-cell/>'   # literally empty cell (to avoid an Err:504)
  end
  rtf = rtfods
  rtfods += 1
  lastt = t

  # -- speed (with configurable fix)
  #
  out.puts "     <table:table-cell office:value-type=\"float\" office:value=\"#{speed.to_f * speedfix}\" calcext:value-type=\"float\">"
  out.puts "      <text:p>#{'%.2f' % [ speed.to_f * speedfix ]}</text:p>"
  out.puts '     </table:table-cell>'

  # -- acceleration (as is)
  #
  out.puts "     <table:table-cell office:value-type=\"float\" office:value=\"#{acceleration}\" calcext:value-type=\"float\">"
  out.puts "      <text:p>#{acceleration}</text:p>"
  out.puts '     </table:table-cell>'

  # -- battery, reported as an integer
  #
  out.puts "     <table:table-cell office:value-type=\"percentage\" office:value=\"#{battery.to_f/100.0}\" calcext:value-type=\"percentage\">"
  out.puts "      <text:p>#{battery}%</text:p>"
  out.puts '     </table:table-cell>'

  # -- safety margin, reported as an integer
  #
  out.puts "     <table:table-cell office:value-type=\"percentage\" office:value=\"#{safety.to_f/100.0}\" calcext:value-type=\"percentage\">"
  out.puts "      <text:p>#{safety}%</text:p>"
  out.puts '     </table:table-cell>'

  # -- alarm value (bit mapped), as encoded by euc.world app:
  #     1 - 3rd (highest priority) speed alarm
  #     2 - 2nd (medium priority) speed alarm
  #     4 - 1st (lowest priority) speed alarm
  #     8 - peak current alarm
  #    16 - sustained current alarm
  #    32 - temperature alarm
  #    64 - overvoltage alarm
  #   128 - data marker event (not an alarm)
  #   256 - safety margin alarm
  #   512 - speed limit alarm
  #  1024 - undervoltage alarm
  #
  if alarm == '0'
    out.puts '     <table:table-cell/>'   # literally empty cell
  else
    out.puts "     <table:table-cell office:value-type=\"float\" office:value=\"#{alarm}\" calcext:value-type=\"float\">"
    out.puts "      <text:p>#{alarm}</text:p>"
    out.puts '     </table:table-cell>'
  end

  if gps_lat == nil || gps_lat == '' || gps_lon == ''
    out.puts '     <table:table-cell/>'   # literally empty cell
  else
    escapedlink = MAPSLINK.sub('&', '&amp;')
    out.puts "     <table:table-cell table:style-name=\"ce33\" table:formula=\"of:=HYPERLINK(&quot;#{escapedlink}#{gps_lat},#{gps_lon}&quot;; &quot;map&quot;)\" office:value-type=\"string\" office:string-value=\"map\" calcext:value-type=\"string\">"
    out.puts '      <text:p>map</text:p>'
    out.puts '     </table:table-cell>'
  end

  if gps_speed == nil || gps_speed == '' || gps_speed == '0' || gps_speed == '0.00'
    out.puts '     <table:table-cell/>'   # literally empty cell
  else
    out.puts "     <table:table-cell office:value-type=\"float\" office:value=\"#{gps_speed}\" calcext:value-type=\"float\">"
    out.puts "      <text:p>#{'%.1f' % [ gps_speed.to_f ]}</text:p>"
    out.puts '     </table:table-cell>'
  end

  if gps_alt == nil || gps_alt == ''
    out.puts '     <table:table-cell/>'   # literally empty cell
  else
    out.puts "     <table:table-cell office:value-type=\"float\" office:value=\"#{gps_alt}\" calcext:value-type=\"float\">"
    out.puts "      <text:p>#{'%.0f' % [ gps_alt.to_f ]}</text:p>"
    out.puts '     </table:table-cell>'
  end

  out.puts "     <table:table-cell office:value-type=\"float\" office:value=\"#{speedlimit}\" calcext:value-type=\"float\">"
  out.puts "      <text:p>#{'%.2f' % [ speedlimit.to_f ]}</text:p>"
  out.puts '     </table:table-cell>'

  out.puts '    </table:table-row>'
end

out.puts a
while a = fods.gets             # copy until footer ends
  out.puts a
end

# also suggest how to start libreoffice calc to load the .fods file
# (don't forget to press ctrl-shift-f9 to fully recalculate the spreadsheet)
#
STDERR.puts "localc #{fname} &"
STDERR.puts

# --- the end ---
