#!/usr/bin/env ruby
#
if ARGV.size > 0
  STDERR.puts "!--#$0 -- converts euc.world log file to garmin 3-fields csv waypoint file loadable from gpxsee.org"
  exit 1
end

eucfields = gets.chomp.split ','
if eucfields.first != 'datetime'
  STDERR.puts "!--input file does not look like an euc.world log file"
  exit 2
end

csvgarmin = []
eucfields.each { |e| e.prepend '@' }
while a = gets
  eval "#{eucfields.join ','} = #{a.chomp.inspect}.split ','"
  next unless @gps_lat
  next if @gps_lat == ''
  next if @datetime == 'datetime'

  csvgarmin << [ @gps_lon, @gps_lat, '""' ].join(',')
end

csvgarmin.sort!.uniq!
puts csvgarmin
STDERR.puts "!--#{csvgarmin.size} records"

