/*
//  hs1000.c  release 3 (29-dec-1999)                 under GNU license (GPL2)
//
//  copyright (C) 1996-1999  Alfonso Martone             a.martone@retepnet.it
//
//  compile under Linux 1.2.x and Linux 2.0.x:
//     cc -c -O2 -fomit-frame-pointer -Wall -Wstrict-prototypes -D__KERNEL__ \
//                                    -DKERNEL_VERSION=\"`uname -r`\" hs1000.c
//  compile under Linux 2.2.x:
//     cc -c -O2 -fomit-frame-pointer -Wall -Wstrict-prototypes -D__KERNEL__ \
//                          -fno-strict-aliasing -fno-strength-reduce hs1000.c
//
// ---------------------------------------------------------------------------
//    object:   Linux (i386) driver for the DFI Handy Scanner HS-1000
//
//    I got this scanner in autumn 1988, and it always ran 100% OK from my
//    first 8MHz 8088 PC/XT clone to my actual 500MHz overclocked Celeron
//    Mendocino PC clone today. Why not use it under Linux?
//
//    The scanner has a 200dpi fixed resolution; using it is very simple:
//    only a command to enable/disable scanner and the rest is DMA-stuff.
//    On the scanner there is a 4-positions switch for pseudo-grays and a
//    light-sensitivity level tab. None of these is controlled by software.
//
//    It comes with a simple 8-bit PC/XT-style controller card, a DOS utility
//    SCAN.EXE on a 5.25" 360k floppy disk. The scanner uses DMA transfers
//    on DMA channel 3; there is a little jumper on the card to select DMA
//    channel 1. By default, this driver uses DMA #3: check in the first
//    #define's how to change it.
//
// ---------------------------------------------------------------------------
//    using the driver:
//
//    Just create the entry /dev/scanner (mknod /dev/scanner c 26 0)
//    and insert the module (insmod hs1000.o). Then you can read from it:
//
//      dd if=/dev/scanner of=filename.bin bs=64b count=40
//
//    Second example: let's read again 40 lines, this time from a C program:
//
//      FILE *fp;
//      char buffer[40*64];
//    ...
//      fp=fopen("/dev/scanner", "rb");
//      fread(buffer, 64, 40, fp);
//    ...
//
//    Since it uses 0x27a-0x27b I/O address space, maybe it's not compatible
//    with some secondary parallel ports: some PCs that check those addresses
//    at boot time leave the scanner turned on.
//
// ---------------------------------------------------------------------------
//    software releases:
//
//    release 1  31-aug-1996  for Linux 1.2.x
//    release 2  20-dec-1997  for Linux 2.0.x
//    release 3  29-dec-1999  for Linux 2.2.x
//
*/

#define HS1000_NAME        "hs1000"

#define SCANNER_NAME       "hs1000 scanner"
#define SCANNER_MAJOR      26

#ifndef USE_DMA
#define USE_DMA            1             /*  use this DMA channel (1 or 3)  */
#endif

#define BUFFERSIZE         (4*HS1000_BPL)        /*  4 scanlines at a time  */

#define TIMEOUT            375       /*  max 3.75 sec 2wait4 scanner reads  */


/* --- the following #define's are *not* config options ------------------- */

#define MODULE                       /*  it runs only if loaded as module   */

#define HS1000_BPL         64        /*  64 bytes per line == 512 pixels    */

#define HS1000_DMA         USE_DMA   /*  DMA channel used by the scanner    */

#define HS1000_BASE        0x27a     /*  base address of the scanner        */
#define HS1000_PORTS       2         /*  number of ports to request         */

#define HS1000_COMMAND     0x27a     /*  command port and known commands    */
#define    HS1000_SET_ON   0x0d
#define    HS1000_SET_OFF  0x0c

#define HS1000_STATUS      0x27b     /*  status port and known commands     */
#define    HS1000_CLRSTAT  0xff

#define turn_scanner_on()  outb(HS1000_SET_ON,  HS1000_COMMAND)
#define turn_scanner_off() outb(HS1000_SET_OFF, HS1000_COMMAND)

                                     /*  the check for the DMA completion:  */
#if USE_DMA==1
#define dma_complete()     ((inb(DMA1_STAT_REG) & (1 << HS1000_DMA)))
#define DMA_INFO           "(on DMA1)"
#else
#define dma_complete()     ((inb(DMA1_STAT_REG) & (1 << HS1000_DMA)))
#define DMA_INFO           "(on DMA3)"
#endif

/* ------------------------------------------------------------------------ *\
|  no one should really have the need of modifying anything below this line  |
\* ------------------------------------------------------------------------ */

#include <linux/sched.h>
#include <linux/errno.h>
#include <linux/kernel.h>
#include <linux/malloc.h>
#include <linux/ioport.h>
#include <linux/module.h>

#define REALLY_SLOW_IO
#include <asm/io.h>
#include <asm/dma.h>
#include <asm/segment.h>

/* --- kernel dependent stuff --------------------------------------------- */

#if LINUX_VERSION_CODE < 0x020000
char kernel_version[]=KERNEL_VERSION;         /*  required by Linux 1.2.x  */
#endif


#if LINUX_VERSION_CODE < 0x020100
#define signal_pending(current) (current->signal & ~current->blocked)
#endif


#if LINUX_VERSION_CODE < 0x02017f
void schedule_timeout(int j)
{
    current->state = TASK_INTERRUPTIBLE;       /*  give up some cpu time    */
    current->timeout = jiffies + j;
    schedule();                                /*  schedule other tasks     */
}
#endif


#define sfile   struct file
#define sinode  struct inode

#if LINUX_VERSION_CODE < 0x020200           /*  old file_operations struct  */

#define hsopen()  int HS1000_open(sinode *inode, sfile *file)
#define hsread()  int HS1000_read(sinode *i, sfile *s, char *buf, int rq)
#define hsseek()  int HS1000_lseek(sinode *i, sfile *s, off_t loff, int u)
#define hspoll()  int HS1000_sel(sinode *i, sfile *s, int, select_table *z)
#define hsclose() void HS1000_close(sinode *i, sfile *file)

#define hsclose_ok()     /* function returning void */
#define return_poll_sel  return 1

#else                                       /*  new file_operations struct  */

#define hsopen()  int HS1000_open(sinode *inode, sfile *file)
#define hsread()  ssize_t HS1000_read(sfile *f,char *buf,size_t rq,loff_t *l)
#define hsseek()  long long HS1000_lseek(sfile *f, loff_t loff, int u)
#define hspoll()  unsigned int HS1000_sel(sfile *f, poll_table *po)
#define hsclose() int HS1000_close(sinode *inode, sfile *file)

#define hsclose_ok()     return 0
#define return_poll_sel  return POLLIN | POLLRDNORM

#include <linux/poll.h>
#include <asm/uaccess.h>
#define memcpy_tofs copy_to_user

#endif


/* ------------------------------------------------------------------------ *\
   variables and internal functions...
\* ------------------------------------------------------------------------ */

static char *buffer;                           /*  the scanner line buffer  */
static char  active=0;                         /*  active/inactive module   */
static char  reading=0;                        /*  reading/standby mode     */


static void scan_start(void)    /*  setup DMA transfer and start read-mode  */
{
  cli();
  disable_dma(HS1000_DMA);                          /*  setup DMA transfer  */
  set_dma_mode(HS1000_DMA, DMA_MODE_READ);
  clear_dma_ff(HS1000_DMA);
  set_dma_addr(HS1000_DMA, (int)buffer);           /*  within GFP_DMA area  */
  set_dma_count(HS1000_DMA, BUFFERSIZE);
  enable_dma(HS1000_DMA);

  outb(HS1000_CLRSTAT, HS1000_STATUS);      /*  these two instructions      */
  inb(HS1000_COMMAND);                      /*    activate scanner-reading  */

  reading=1;                                /*  now: ready to accept data   */
  sti();
}


/* ------------------------------------------------------------------------ *\
   scanner reading, only if fits in the buffer
\* ------------------------------------------------------------------------ */


static hsopen()   /*  "open" call: turn on the scanner and prepare to read  */
{
  register int r;
  if(active) return -EBUSY;

  turn_scanner_on();                               /*  turn on the scanner  */
  inb(HS1000_STATUS);

  if((r=request_dma(HS1000_DMA, SCANNER_NAME)))
  {
    turn_scanner_off();
    return r;
  }
  disable_dma(HS1000_DMA);    /*  DMA channel is also disabled on the 8237  */
  reading = 0;
  active = 1;

  MOD_INC_USE_COUNT;
  return 0;                /*  device open, scanner is in standby mode now  */
}


static hsread()   /*  "read" call: get a scanline (if possible) and return  */
{
  register unsigned long timeout=jiffies+TIMEOUT;
  register int requested=rq;
  if(requested<BUFFERSIZE) return(0);          /*  ignore tiny requests     */

  while(jiffies<timeout)                       /*  waiting for some data:   */
  {
    if(!reading) scan_start();                 /*  start if not already...  */
    if(dma_complete())
    {                                          /*  if one read completed:   */
      cli();                                   /*  cli (don't stop me!)     */
      memcpy_tofs(buf, buffer, BUFFERSIZE);    /*  xfer data to user space  */
      scan_start();                            /*  start next transfer and  */
      return BUFFERSIZE;                       /*    ret with ints enabled  */
    }
    schedule_timeout(2);                       /*  give up 0.02 seconds...  */
    if(signal_pending(current)) break;         /*  got a signal? yes: exit  */
  }
  return 0;
}


static hsclose()                     /*  "close" call: turn off everything  */
{
  disable_dma(HS1000_DMA);
  free_dma(HS1000_DMA);
  turn_scanner_off();
  active = 0;
  MOD_DEC_USE_COUNT;
  hsclose_ok();
}


static hspoll()   /*  "select" or "poll" call, depending on kernel version  */
{
  return_poll_sel;           /*  return "scanner is always ready for read"  */
}


static hsseek()                                /*  "seek" call: do nothing  */
{
  return -ESPIPE;                           /*  you can't fseek a scanner!  */
}


static struct file_operations HS1000_fops =   /*  map of defined functions  */
{
  HS1000_lseek,
  HS1000_read,
    NULL,           /*  write    */
    NULL,           /*  readdir  */
  HS1000_sel,       /*  select (before Linux 2.2) or poll (from Linux 2.2)  */
    NULL,           /*  ioctl    */
    NULL,           /*  mmap     */
  HS1000_open,
#if LINUX_VERSION_CODE >= 0x020200
    NULL,           /*  flush, from Linux 2.2 only  */
#endif
  HS1000_close,
};


int init_module(void)
{
  printk("HS1000 module driver release 3 " DMA_INFO "; by Alfonso Martone "
    "(a.martone@retepnet.it)\n");

  buffer=(char*)kmalloc(BUFFERSIZE, GFP_KERNEL | GFP_DMA);
  if(buffer==NULL) return -ENOMEM;

  if(register_chrdev(SCANNER_MAJOR, SCANNER_NAME, &HS1000_fops))
  {
    printk("hs1000.c: cannot register major number\n");
    kfree_s(buffer, BUFFERSIZE);
    return -EIO;
  }

  request_region(HS1000_BASE, HS1000_PORTS, SCANNER_NAME);

  turn_scanner_on();
  turn_scanner_off();
  return 0;
}


void cleanup_module(void)
{
  if(MOD_IN_USE)
    printk("hs1000.c: cleanup_module() requested while module in use\n");

  if(unregister_chrdev(SCANNER_MAJOR, SCANNER_NAME))
    printk("hs1000.c: unregister_chrdev() failed\n");

  turn_scanner_on();
  turn_scanner_off();

  kfree_s(buffer, BUFFERSIZE);
  release_region(HS1000_BASE, HS1000_PORTS);
  printk("HS1000 module driver removed\n");
}


/* --- the end ------------------------------------------------------------ */
