/*
//  linux/drivers/char/scanner.c                             under GNU license
//
//  copyright (C) 1996  Alfonso Martone
//
// ---------------------------------------------------------------------------
//    object:   Linux driver for the DFI Handy Scanner HS-1000
// ---------------------------------------------------------------------------
//
//    yeah, my HS1000 scanner is dated April 1988, but it's still functional:
//    why not use it under Linux? have a look at README file for more info.
//
//    release 1.0:  31-aug-96  Linux 1.2.xx version
//            1.1:  20-dec-97  Linux 2.0.xx version
//
*/

#define MODULE

#define HS1000_NAME          "hs1000"

#define SCANNER_NAME         "scanner"
#define SCANNER_MAJOR        26

#define BUFFERSIZE           (4*HS1000_BPL)      /*  4 scanlines at a time  */

#define TIMEOUT              6       /*  max 0.06 sec 2wait4 scanner reads  */

/* --- the following #define's are *not* config options ------------------- */

#define HS1000_BPL           64      /*  64 bytes per line == 512 pixels    */

#define HS1000_DMA           1       /*  DMA channel used by the scanner    */

#define HS1000_BASE          0x27a   /*  base address of the scanner        */
#define HS1000_PORTS         2       /*  number of ports to request         */

#define HS1000_COMMAND       0x27a   /*  command port and known values (!)  */
#define    HS1000_SET_ON     0x0d
#define    HS1000_SET_OFF    0x0c

#define HS1000_STATUS        0x27b   /*  status port and known values       */
#define    HS1000_CLRSTAT    0xff

/*  the check for the DMA completion  */
#define dma_complete()        ((inb(DMA1_STAT_REG) & (1 << HS1000_DMA)))

/* ------------------------------------------------------------------------ *\
|  no one should really have the need of modifying anything below this line  |
\* ------------------------------------------------------------------------ */

#include <linux/sched.h>
#include <linux/errno.h>
#include <linux/kernel.h>
#include <linux/malloc.h>
#include <linux/ioport.h>
#include <linux/module.h>

#define REALLY_SLOW_IO

#include <asm/io.h>
#include <asm/dma.h>
#include <asm/segment.h>


/* ------------------------------------------------------------------------ *\
   variables and constants...
\* ------------------------------------------------------------------------ */

static char *buffer;                          /*  the scanner line buffer  */
static char  active=0;                        /*  active/inactive module   */
static char  reading=0;                       /*  reading/standby mode     */

/* char kernel_version[]=KERNEL_VERSION;      //  needed for Linux 1.2.xx  */

static void scan_start(void)
{
  cli();
  disable_dma(HS1000_DMA);                          /*  setup DMA transfer  */
  set_dma_mode(HS1000_DMA, DMA_MODE_READ);
  clear_dma_ff(HS1000_DMA);
  set_dma_addr(HS1000_DMA, (int)buffer);
  set_dma_count(HS1000_DMA, BUFFERSIZE);
  enable_dma(HS1000_DMA);
  outb(HS1000_CLRSTAT, HS1000_STATUS);      /*  these two instructions      */
  inb(HS1000_COMMAND);                      /*    activate scanner-reading  */
  reading=1;
  sti();
}


/* ------------------------------------------------------------------------ *\
   scanner reading, only if fits in the buffer
\* ------------------------------------------------------------------------ */

static int HS1000_read(struct inode *inode, struct file *file,
        char *buf, int requested)
{
  register unsigned long timeout=jiffies+TIMEOUT;
  if(requested<BUFFERSIZE) return(0);

  while(jiffies<timeout)
  {
    if(!reading) scan_start();                 /*  start if not already...  */
    if(dma_complete())
    {
      cli();                                   /*  don't stop me!           */
      memcpy_tofs(buf, buffer, BUFFERSIZE);    /*  xfer data to user space  */
      scan_start();                            /*  start next transfer and  */
      return BUFFERSIZE;                       /*    ret with ints enabled  */
    }
    current->state = TASK_INTERRUPTIBLE;       /*  give up some cpu time    */
    current->timeout = jiffies + 2;            /*  0.02 seconds...          */
    schedule();                                /*  schedule other tasks     */

    if(current->signal & ~current->blocked) break;       /*  got a signal?  */
  }
  return 0;
}


static int HS1000_open(struct inode *inode, struct file *file)
{
  register int r;
  if(active) return -EBUSY;

  outb(HS1000_SET_ON, HS1000_COMMAND);             /*  turn on the scanner  */
  inb(HS1000_STATUS);

  if((r=request_dma(HS1000_DMA, SCANNER_NAME)))    /*  request dma channel  */
  {
    outb(HS1000_SET_OFF, HS1000_COMMAND);
    return r;
  }
  disable_dma(HS1000_DMA);    /*  DMA channel is also disabled on the 8237  */
  reading = 0;
  active = 1;

  MOD_INC_USE_COUNT;
  return 0;                /*  device open, scanner is in standby mode now  */
}


static void HS1000_close(struct inode *inode, struct file *file)
{
  disable_dma(HS1000_DMA);
  free_dma(HS1000_DMA);
  outb(HS1000_SET_OFF, HS1000_COMMAND);
  active = 0;
  MOD_DEC_USE_COUNT;
}


static int HS1000_select(struct inode *inode, struct file *file,
        int type, select_table *st)
{
  return 1;                           /*  scanner is always ready for read  */
}


static int HS1000_lseek(struct inode *inode, struct file *file,
        off_t offset, int origin)
{
  return -ESPIPE;                           /*  you can't fseek a scanner!  */
}


static struct file_operations HS1000_fops =
{
  HS1000_lseek,
  HS1000_read,
    NULL,             /* write   */
    NULL,             /* readdir */
  HS1000_select,
    NULL,             /* ioctl   */
    NULL,             /* mmap    */
  HS1000_open,
  HS1000_close,
};


int init_module(void)
{
  printk("HS1000 module driver v1.1  by Alfonso Martone\n");

  buffer=(char*)kmalloc(BUFFERSIZE, GFP_KERNEL | GFP_DMA);
  if(buffer==NULL) return -ENOMEM;

  if(register_chrdev(SCANNER_MAJOR, SCANNER_NAME, &HS1000_fops))
  {
    printk("hs1000.c: cannot register major number\n");
    kfree_s(buffer, BUFFERSIZE);
    return -EIO;
  }

  request_region(HS1000_BASE, HS1000_PORTS, HS1000_NAME "(" SCANNER_NAME ")");
  return 0;
}


void cleanup_module(void)
{
  if(MOD_IN_USE)
    printk("hs1000.c: device is busy, driver removal should be delayed\n");

  if(unregister_chrdev(SCANNER_MAJOR, SCANNER_NAME))
    printk("hs1000.c: unregister_chrdev() failed\n");

  kfree_s(buffer, BUFFERSIZE);
  release_region(HS1000_BASE, HS1000_PORTS);
  printk("HS1000 module driver removed\n");
}


/* --- */
